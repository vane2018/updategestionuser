const sqlite3 = require('sqlite3');
const express = require("express");

const bodyParser = require('body-parser')

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const HTTP_PORT = 8000
app.listen(HTTP_PORT, () => {
    console.log("Server the Ana is listening on port " + HTTP_PORT);
});

const db = new sqlite3.Database('./user.db', (err) => {
    if (err) {
        console.error("Erro opening database " + err.message);
    } else {

        db.run('CREATE TABLE users( \
            user_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\
            name NVARCHAR(20)  NOT NULL,\
            last_name NVARCHAR(20)  NOT NULL,\
            birthday DATE,\
            dni NVARCHAR(10)\
        )', (err) => {
            if (err) {
                console.log("Table already exists.");
            }
            let insert = 'INSERT INTO users (name, last_name, birthday, dni) VALUES (?,?,?,?)';
            db.run(insert, ["Chandan", "Praveen", "23-03-02", "3467485"]);

        });
    }
});

app.get("/users/:id", (req, res, next) => {
    var params = [req.params.id]
    db.get("SELECT * FROM users where user_id = ?", [req.params.id], (err, row) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.status(200).json(row);
    });
});

app.get("/users", (req, res, next) => {
    db.all("SELECT * FROM users", [], (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.status(200).json({ rows });
    });
});

app.post("/searchBetweenFecha", (req, res, next) => {
    let inicio = "'" + req.body.inicio + "'";
    let fin = "'" + req.body.fin + "'";
    db.all("SELECT * FROM users WHERE birthday >= '" + req.body.inicio + "' and birthday <= '" + req.body.fin + "' ", [], (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.status(200).json({ rows });
    });
});

app.post("/beforeFecha", (req, res, next) => {
    db.all("SELECT * FROM users WHERE birthday <= '" + req.body.inicio + "'", [], (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.status(200).json({ rows });
    });
});

app.post("/afterFecha", (req, res, next) => {
    db.all("SELECT * FROM users WHERE birthday >= '" + req.body.inicio + "' ", [], (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.status(200).json({ rows });
    });
});

app.post("/searchCadena", (req, res, next) => {
    db.all("SELECT * FROM users WHERE ((name LIKE '%" + req.body.name + "%') || (last_name LIKE '%" + req.body.name + "%'))", [], (err, rows) => {
        if (err) {
            res.status(400).json({ "error": err.message });
            return;
        }
        res.status(200).json({ rows });
    });
});


app.post("/users", function (req, res) {
    console.log("entra")
    console.log(req.body.last_name);
    var reqBody = req.body;

    db.run("INSERT INTO users (name, last_name, birthday,dni) VALUES (?,?,?,?)",
        [reqBody.name, reqBody.last_name, reqBody.birthday, reqBody.dni],
        function (err, result) {
            if (err) {
                res.status(400).json({ "error": err.message })
                return;
            }
            res.status(201).json({
                "user_id": this.lastID
            })
        });
});

app.patch("/users/", (req, res, next) => {
    var reqBody = req.body;
    db.run(`UPDATE users set name = ?, last_name = ?, birthday = ?, dni = ? WHERE user_id = ?`,
        [reqBody.name, reqBody.last_name, reqBody.birthday, reqBody.dni, reqBody.user_id],
        function (err, result) {
            if (err) {
                res.status(400).json({ "error": res.message })
                return;
            }
            res.status(200).json({ updatedID: this.changes });
        });
});

app.delete("/users/:id", (req, res, next) => {
    db.run(`DELETE FROM users WHERE user_id = ?`,
        req.params.id,
        function (err, result) {
            if (err) {
                res.status(400).json({ "error": res.message })
                return;
            }
            res.status(200).json({ deletedID: this.changes })
        });
});